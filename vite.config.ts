import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  
  const env = loadEnv(mode, process.cwd(), '')
  
  return {
    base: env.APP_BASE_URL || 'https://StavrosIoannidis.gitlab.io/yuty-widgets/',
    plugins: [vue()],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    build: {
      rollupOptions: {
        output: {
          dir: 'dist',
          entryFileNames: 'index.js',
          assetFileNames: 'assets/[name].[ext]',
          chunkFileNames: "chunk.js",
          manualChunks: undefined,
        }
      },
      sourcemap: true,
      target: 'esnext'
    }
  }
});
