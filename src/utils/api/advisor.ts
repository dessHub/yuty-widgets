import axios from 'axios';

const DSML_AUTHORIZATION_TOKEN = 'Token 199b59b2ca05013b507c1ca0aab4ebfbeee45a8f';

const getReco = async (id: string) => {
  const backendUrl = 'https://tqzwo26fpbhvlu34raqz6xrmy40ikixp.lambda-url.us-west-2.on.aws/';
  const url = backendUrl.concat(`v1/makeup_engine/${id}`);

  try {

    const result = await axios.get(url, {
      headers: {
        'Content-Type': 'multipart/form-data;',
        'Authorization': DSML_AUTHORIZATION_TOKEN
      }
    });
    const reco = result.data;
    return reco;
  } catch (e) {
    console.error(e);
    throw e;
  }
};


const skinAdvisorImage =  async (quizId: string, imageData: Blob) => {
  const backendUrl = 'https://tkyhkf7tsf5ihtnjatyztmb5ny0atcxu.lambda-url.us-west-2.on.aws/v1';
  const url = backendUrl.concat('/skin_image');

  try {
    const data = new FormData();
    data.append('quiz_id', quizId);
    data.append('file', imageData);

    const result = await axios.post(url, data, {
      headers: {
        'Content-Type': 'multipart/form-data;',
        Authorization: DSML_AUTHORIZATION_TOKEN
      }
    });
    const analysis = result.data;
    return analysis;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

const makeupAdvisorImage =  async (quizId: string, imageData: Blob) => {
  const backendUrl = 'https://tqzwo26fpbhvlu34raqz6xrmy40ikixp.lambda-url.us-west-2.on.aws/v1';
  const url = backendUrl.concat('/upload_image');

  try {
    const data = new FormData();
    data.append('quiz_id', quizId);
    data.append('file', imageData);

    const result = await axios.post(url, data, {
      headers: {
        'Content-Type': 'multipart/form-data;',
        Authorization: DSML_AUTHORIZATION_TOKEN
      }
    });
    const analysis = result.data;
    return analysis;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

export default {getReco, skinAdvisorImage, makeupAdvisorImage}

