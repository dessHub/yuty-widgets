import advisor from "./advisor";
import helpers from "./helpers";
import products from "./products";
import questions from "./questions";
import quiz from "./quiz";
import shopify from "./shopify";
import wishlist from "./wishlist";

export const getReco = advisor.getReco;
export const skinAdvisorImage = advisor.skinAdvisorImage;
export const makeupAdvisorImage = advisor.makeupAdvisorImage;
export const getIngredients = helpers.getIngredients;
export const getCities = helpers.getCities;
export const getRecommendedProducts = products.getRecommendedProducts;
export const getQuestions = questions.getQuestions;
export const createQuiz = quiz.createQuiz;
export const updateAnswer = quiz.updateAnswer;
export const getStoreProducts = shopify.getProducts;
export const getCart = shopify.getCart;
export const addToCart = shopify.addToCart;
export const createWishlist = wishlist.createWishlist;
