import axios from 'axios';

const DSML_AUTHORIZATION_TOKEN = 'Token 199b59b2ca05013b507c1ca0aab4ebfbeee45a8f';

const getIngredients = async (query: any) => {
  if (!query) return [];
  const elasticsearchUrl = 'https://api.yutybazar.com';
  const url = `${elasticsearchUrl}/api/autocomplete_ingredients/${query}`;

  try {
    const result = await axios.get(url, { headers: { Authorization: DSML_AUTHORIZATION_TOKEN } });
    return result.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

const getCities = async (query: any) => {
  if (!query) return [];
  const elasticsearchUrl = 'https://api.yutybazar.com';
  const url = `${elasticsearchUrl}/api/autocomplete_cities/${query}`;

  try {
    const result = await axios.get(url, { headers: { Authorization: DSML_AUTHORIZATION_TOKEN } });
    return result.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export default { getIngredients, getCities }
