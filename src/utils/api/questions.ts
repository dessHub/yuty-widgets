import axios from 'axios';
import { deserializeQuestions } from '../serializers/quiz';
import type { QuizQuestion } from '@/utils/types';

const include = 'answer_options';
const baseUrl = 'https://api.yuty.me';

const getQuestions = async (type: string): Promise<QuizQuestion[]> => {
  const endpoint = baseUrl.concat('/api/v2/storefront/questions');

  try {
    const result = await axios.get(endpoint, {
      params: {
        include,
        sort: 'position',
        'filter[quiz_type]': type
      }
    });
    const { data, included } = result.data;
    return deserializeQuestions(data, included);
  } catch (error) {
    console.error(error);
    throw error;
  }
}


export default { getQuestions };