import axios from 'axios';
import type { WishedProduct } from '@/utils/types';

const createWishlist = async (products: WishedProduct[], email: string, brand: string) => {
  try {
    const backendUrl = 'https://api-staging.yuty.me';
    const url = backendUrl.concat('/api/v2/storefront/widget_wishlist');

    const data = {
        products,
        email,
        brand
    }
    const result = await axios.post(url, data);

    return result.data;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export default { createWishlist };