import axios from 'axios';
import { deserializeRecommendedProducts } from "../serializers/products";

const getRecommendedProducts = async (ids: number[]) => {
  try {
    const backendUrl = 'https://api-staging.yuty.me';
    const url = backendUrl.concat('/api/v2/storefront/products');

    const result = await axios.get(url, {
      params: {
        currency: 'USD',
        filter: {
          ids: ids.join(',')
        },
        fields: {
          product: 'name,short_description,images,spree_brand,slug,price,display_price,in_stock,default_variant,variants',
          variant: 'id,display_price'
        },
        include: 'images,spree_brand,variants',
        page: 1,
        // eslint-disable-next-line camelcase
        per_page: 100
      }
    });

    const { data, included } = result.data;
    return deserializeRecommendedProducts(data, included);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

export default { getRecommendedProducts };