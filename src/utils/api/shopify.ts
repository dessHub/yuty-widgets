import axios from 'axios';
import type { StoreProductDetails } from '../types';

const SHOPIFY_AUTHORIZATION_TOKEN = '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' //'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';
const store = 'shoe28' //'colorblend-makeup-london';
const url = `https://${store}.myshopify.com/admin/api/2023-01/products.json`

const getProducts = async () => {

  try {
    const result = await axios.get(url, { headers: { 'X-Shopify-Access-Token': SHOPIFY_AUTHORIZATION_TOKEN } });
    return result.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

const getCart = async () => {
  const result = await fetch("/cart.json");

  if (result.status === 200) {
      return result.json();
  }

  throw new Error(`Failed to get request, Shopify returned ${result.status} ${result.statusText}`);
}

const addToCart = async (reco: StoreProductDetails[]) => {

  const items = reco.map(({variants}) => ({id: variants[0].id, quantity: 1}));

  let formData = {
    'items': [...items]
   };

  const result = await fetch("/cart/add.js", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(formData)
  });

  if (result.status === 200) {
      return result.json();
  }

  throw new Error(`Failed to add to cart, Shopify returned ${result.status} ${result.statusText}`);
}

export default { getProducts, getCart, addToCart };