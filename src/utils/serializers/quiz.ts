/* eslint-disable camelcase */

import type { AnswerRaw, QuestionInclude, QuestionRaw, QuizAnswer, QuizQuestion } from '../types';

const deserializeAnswers = (answers: AnswerRaw[]): QuizAnswer[] =>
  answers.map((answer) => ({
    id: answer.id,
    text: answer.attributes.text,
    position: answer.attributes.position,
    description: answer.attributes.description,
    image: answer.attributes.image
  }));

const extractAnswers = (question: QuestionRaw, included: QuestionInclude[]) => {
  const answerIds = question.relationships.answer_options.data.map((e) => e.id);
  return included.filter((e) => answerIds.includes(e.id));
};

export const deserializeQuestions = (data: QuestionRaw[], included: QuestionInclude[]): QuizQuestion[] => {
  const questions = data.map((question) => ({
    id: question.id,
    text: question.attributes.text,
    position: question.attributes.position,
    multipleAnswer: question.attributes.multiple_answer,
    answersCount: question.attributes.answers_count,
    unlimited: question.attributes.unlimited,
    additionalInfo: question.attributes.extra_info,
    freetext: question.attributes.freetext,
    slug: question.attributes.slug,
    isAllergen: question.attributes.is_alergen,
    isCity: question.attributes.is_city,
    answers: deserializeAnswers(extractAnswers(question, included))
  }));

  return questions;
};
