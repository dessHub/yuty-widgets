export type OptionValue = {
  id: number;
  type: string;
  attributes?: {
    name: string;
    position: number;
    presentation: string;
  };
  relationships?: {
    // eslint-disable-next-line camelcase
    option_type: {
      data: {
        id: number;
        type: string;
      };
    };
  };
};

export type OptionType = {
  id: number;
  type: string;
  attributes?: {
    name: string;
    position: number;
    presentation: string;
  };
  relationships?: {
    // eslint-disable-next-line camelcase
    option_values: {
      data: OptionValue[];
    };
  };
};

export type ImageStyle = {
  height: string;
  width: string;
  url: string;
};

export type Image = {
  id: number;
  type: string;
  attributes?: {
    // eslint-disable-next-line camelcase
    viewable_id: number;
    // eslint-disable-next-line camelcase
    viewable_type: string;
    styles: ImageStyle[];
  };
};

export type QuizAnswer = {
  id: string;
  text: string;
  position: number;
  description: string;
  image: string | null;
};

export type QuizQuestion = {
  id: string;
  text: string;
  position: number;
  multipleAnswer: boolean;
  answersCount: number | null;
  unlimited: boolean;
  freetext: boolean;
  isAllergen: boolean;
  isCity: boolean;
  slug: string;
  answers: QuizAnswer[];
};

export type Answer = {
    id: string,
    type: string,
    isImage: boolean,
    isFreetext: boolean,
    answers: [] | string
};

export type Feedback = {
  message: string;
  rating: string;
};

export interface Product {
  id: number,
  name: string,
  subname: string,
  price: number,
  currency: string,
  concerns: string,
  actives: string,
  functions: string,
  usage: string,
  image: string
}

export interface RecommendedProduct {
  product: Product,
  quantity: number
}

export type User = {
  // eslint-disable-next-line camelcase
  first_name: string;
  // eslint-disable-next-line camelcase
  sur_name: string;
  email: string;
  birthday: Date;
  password: string;
};

export interface QuantityEvent {
  type: string,
  index: number
}

export interface Include {
  id?: string,
  type?: string,
  [key: string]: any
}

export interface VariantDetails {
  displayPrice: string,
  _id: string
}

export interface RecommendedProductDetails {
  _id: string,
  _variantId: string,
  variantIds: string[],
  name: string,
  shortDescription: string,
  images: Include[],
  brand: Include,
  variants: VariantDetails[],
  slug: string,
  price: string,
  displayPrice: string,
  inStock: boolean
}

export interface StoreProductDetails {
  id: number,
  variants: StoreProductVariant[],
  handle: string,
  title: string,
  image: {[key: string]: any},
  images: {[key: string]: any}[],
  [key: string]: any,
}

export interface StoreProductVariant {
  id: number,
  product_id: number,
  [key: string]: any
}

export interface ProductDetails {
  id: string,
  type: string,
  attributes: ProductAttributes,
  relationships: ProductRelationships
}

export interface ProductAttributes {
  name: string,
  description: string,
  price: string,
  currency: string,
  display_price: string,
  in_stock: boolean,
  slug: string,
  [key: string]: any
}

export interface ProductRelationships {
  [key: string]: any
}
export interface AnswerRaw {
  id: string,
  attributes: AnswerAttributes,
}

export interface AnswerAttributes {
  text: string,
  position: number,
  description: string,
  image: string | null
}

export interface QuestionRaw {
  id: string,
  type: string,
  attributes: QuestionAttributes,
  relationships: QuestionRelationships
}

export interface QuestionAttributes {
  text: string,
  quiz_type: string,
  position: number,
  multiple_answer: false,
  freetext: false,
  answers_count: number | null,
  unlimited: boolean,
  slug: string,
  extra_info: string,
  is_alergen: boolean,
  is_city: boolean
}

export interface QuestionRelationships {
  answer_options: {
    data: any[]
  }
}

export interface QuestionInclude {
  id: string,
  type: string,
  attributes: QuestionIncludeAttributes
}

export interface QuestionIncludeAttributes {
  text: string,
  position: number,
  spree_question_id: number,
  description: string,
  image: string | null
}

export interface ImageAnswer {
  _answers: Answer[],
  imageData: string,
  imageFile: Blob
}

export interface SelfieSectionEvent {
  isCamera: boolean, 
  isLoading: boolean, 
  isAnalysis: boolean
}

export interface AnalysisObject {
  detected_skin_concerns?: string[],
  skin_concerns_colours?: {
    [key: string]: number[]
  },
  recommended_products?: RecoProduct[],
  image_name?: string,
  image_data?: string,
  error_skin_concerns?: string
}

export interface RecoProduct {
  product_name: string,
  product_id: number,
  brand_id: number,
  product_slug: string,
  [key: string]: any
}

export interface RecommendationDetails {
  reco?: any,
  essentials?: string[],
  env_info?: {
        city?: string,
        latitude?: number,
        longitude?: any,
        humidity?: string,
        uv_index?: string,
        air_quality?: string,
        wind?: string,
        water_hardness?: string
  },
  env_context?: {
    uv_context?: string,
    aqi_context?: string,
    humidity_context?: string,
    water_context?: string
  },
  env_functions?: {
    uvi?: any[],
    aqi?: any[],
    humidity?: string[],
    wind?: string[],
    water_hardness?: string[]
  },
  concerns?: string[],
  goals?: string[],
  customer_ing?: string[],
  client_slug?: string
}

export interface WishedProduct {
  slug: string,
  name: string
}
